import React from 'react';
import {getAuth} from '../utils/store.util';
import {withRouter} from 'react-router-dom';

export function withAuthentication(ComponentWrapped) {
    class AuthenticatedComponent extends React.Component {
        isAuthenticated() {
            const auth = getAuth();
            return !!auth;
        }

        componentDidMount() {
            if (!this.isAuthenticated()) {
                this.props.history.push('/login')
            }
        }

        render() {
            return (
                <div>
                    <ComponentWrapped {...this.props}/>
                </div>
            )
        }
    }

    return withRouter(AuthenticatedComponent);
}

export default withAuthentication

export function required(value) {
    if (!value) {
        return 'Campo obrigatório';
    }

    return null;
}

export function min(value, min) {
    if (value && value.length < min) {
        return `No mínimo ${min} caractere(s).`
    }
    return null;
}

export function max(value, max) {
    if (value && value.length > max) {
        return `No mánimo ${max} caractere(s).`
    }
    return null;
}

export function isEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(String(email).toLowerCase())) {
        return 'E-mail inválido';
    }
    return null;
}

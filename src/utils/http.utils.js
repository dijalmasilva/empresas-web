import axios from 'axios';
import {PATH_SIGN_IN} from './constants.utils';
import {clearAuth, getAuth, saveAuth} from './store.util';
import historyCustom from '../history'

const url = `${process.env.REACT_APP_API_SERVER}/api/${process.env.REACT_APP_API_VERSION}`;

export const http = axios.create({
    baseURL: url,
    headers: {
        'Content-Type': 'application/json'
    }
});

http.interceptors.request.use(async (config) => {
    if (!config.url.includes(PATH_SIGN_IN)) {
        const auth = getAuth();
        if (auth) {
            config.headers['access-token'] = auth.accessToken;
            config.headers.client = auth.client;
            config.headers.uid = auth.uid;
        }
    }

    return config;
}, (error) => {
    return Promise.reject(error);
});

http.interceptors.response.use(async (response) => {
    const auth = {
        accessToken: response.headers['access-token'],
        client: response.headers['client'],
        uid: response.headers['uid'],
    };
    saveAuth(auth);
    return response;
}, (error) => {
    if (error.response && error.response.status === 401) {
        clearAuth();
        historyCustom.push('/login');
        const requestConfig = error.config;
        return axios(requestConfig);
    }
    return Promise.reject(error);
});

const AUTH_NAME = 'auth';

export function saveAuth(auth) {
    //utilizar o session storage pois não vi o botão para deslogar.
    sessionStorage.setItem(AUTH_NAME, JSON.stringify(auth));
}

export function getAuth() {
    return JSON.parse(sessionStorage.getItem(AUTH_NAME));
}

export function clearAuth() {
    //sessionStorage.clear();
    return sessionStorage.removeItem(AUTH_NAME);
}

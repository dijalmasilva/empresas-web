// nao achei necessario usar redux ou mobx ou algum outro gerenciador de estado para um fluxo tao pequeno

const TEXT_ITEM = 'text_search';
const FILTER_ITEM = 'filter_search';

const saveStateSearch = (textSearch, filterByType) => {
    sessionStorage.setItem(TEXT_ITEM, textSearch);
    sessionStorage.setItem(FILTER_ITEM, JSON.stringify(filterByType));
};

const getTextSearch = () => {
    return sessionStorage.getItem(TEXT_ITEM);
};

const getFilterSearch = () => {
    return JSON.parse(sessionStorage.getItem(FILTER_ITEM));
};

const clearState = () => {
    sessionStorage.removeItem(TEXT_ITEM);
    sessionStorage.removeItem(FILTER_ITEM);
};

export const SimpleStore = {
    saveStateSearch,
    getTextSearch,
    getFilterSearch,
    clearState
};

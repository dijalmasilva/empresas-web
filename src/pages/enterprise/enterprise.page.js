import './enterprise.style.scss';

import React from 'react';
import {enterpriseService} from '../../services/enterprise/enterprise.service';
import LoadingComponent from '../../components/loading/loading.component';
import Header from '../../components/header/header.component';
import {withAuthentication} from '../../hoc/authentication.hoc';
import Axios from 'axios';

function EnterprisePage(props) {

    const apiUrl = process.env.REACT_APP_API_SERVER;
    const id = props.match.params.id;
    const [enterprise, setEnterprise] = React.useState(null);
    const [loading, setLoading] = React.useState(true);

    React.useEffect(() => {
        if (id && !enterprise) {
            const source = Axios.CancelToken.source();
            enterpriseService.getEnterprise(id, source.token).then(res => {
                setEnterprise(res.data.enterprise);
                setLoading(false);
            }).catch(err => {
                console.error(err);
                setLoading(false);
            });

            return () => source.cancel();
        }
    }, [id, enterprise]);

    if (loading || !enterprise) {
        return (
            <div className="enterprise-page">
                <Header isDetailEnterprise nameEnterprise='Aguarde...'/>
                <div className="content-loading">
                    <LoadingComponent visible/>
                </div>
            </div>
        )
    }

    return (
        <div className="enterprise-page">
            <Header isDetailEnterprise nameEnterprise={enterprise.enterprise_name}/>
            <div className='detail'>
                <div className="card-detail-enterprise">
                    <div className="picture">
                        <img src={`${apiUrl}${enterprise.photo}`} alt={`logo-${enterprise.enterprise_name}`}/>
                    </div>
                    <p className="description">
                        {enterprise.description}
                    </p>
                </div>
            </div>
        </div>
    )
}

export default withAuthentication(EnterprisePage)

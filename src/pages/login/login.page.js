import './login.style.scss';
import React from 'react';
import LoginForm from './components/login-form/login.form';
import {authenticationService} from '../../services/authentication/authentication.service';
import {withRouter} from 'react-router-dom';
import LoadingComponent from '../../components/loading/loading.component';

function LoginPage(props) {

    const [error, setError] = React.useState(null);
    const [loading, setLoading] = React.useState(false);

    const submitForm = (data) => {
        setLoading(true);
        authenticationService.signIn(data).then(() => {
            setLoading(false);
            props.history.push('/home');
        }).catch(err => {
            console.error(err);
            setLoading(false);
            setError('Credenciais informadas são inválidas, tente novamente.')
        })
    };

    return (
        <div className="login">
            <LoadingComponent visible={loading} backdrop/>
            <div className="login-content">
                <img src="/images/logo-home/logo-home.png" alt="ioasys-logo" className="logo-home"/>
                <br/><br/>
                <h1>Bem vindo ao Empresas</h1>
                <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
                <LoginForm onSubmit={submitForm} error={error}/>
            </div>
        </div>
    )
}

export default withRouter(LoginPage)

import './login-form.style.scss'

import React from 'react'
import InputCustom from '../../../../components/input-custom/input-custom.component';
import {required, isEmail, min} from '../../../../utils/validators.util'

const initialData = {
    email: null,
    password: null,
};

function LoginForm({onSubmit = () => null, error}) {

    const [data, setData] = React.useState(initialData);
    const [hiddenPassword, setHiddenPassword] = React.useState(true);

    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(data);
    };

    const validatorEmail = (value) => {
        const resultRequired = required(value);
        if (resultRequired) {
            return resultRequired
        }

        const resultEmail = isEmail(value);
        if (resultEmail) {
            return resultEmail
        }
        return null;
    };

    const validatorPassword = (value) => {
        const resultRequired = required(value);
        if (resultRequired) {
            return resultRequired
        }

        const resultMin = min(value, 6);
        if (resultMin) {
            return resultMin
        }
        return null;
    };

    const formIsValid = () => {
        return !validatorEmail(data.email) && !validatorPassword(data.password);
    };

    const onChangedEmail = (value) => {
        setData({...data, email: value})
    };

    const onChangedPassword = (value) => {
        setData({...data, password: value})
    };

    return (
        <form onSubmit={handleSubmit} className="login-form">
            <InputCustom type="email" initialValue='' onChanged={onChangedEmail} validator={validatorEmail}
                         placeholder="E-mail" prefix={
                <img src="/images/ic-email/ic-email@2x.png" alt="icon-email"/>
            }/>
            <InputCustom type={hiddenPassword ? 'password' : 'text'} initialValue='' onChanged={onChangedPassword} validator={validatorPassword}
                         placeholder="Senha"
                         prefix={
                             <img src="/images/ic-cadeado/ic-cadeado@2x.png" alt="icon-email"/>
                         }
                         suffix={
                             <div onClick={() => setHiddenPassword(!hiddenPassword)} className="password-hidden">
                                 <img src={`/images/eyes/eye-${hiddenPassword ? 'open' : 'hidden'}.png`}
                                      alt='show-password'/>
                             </div>
                         }/>
            {
                error &&
                <h6 className="login-error">{error}</h6>
            }
            <br/>
            <button className="bt-submit" disabled={!formIsValid()}>Entrar</button>
        </form>
    )
}

export default LoginForm

import React from 'react';
import EnterpriseTile from './enterprise-tile.component';

function ListEnterprises({dataSource = [], onSelectedEnterprise}) {

    return (
        <div className="enterprises-list">
            {
                dataSource.map(enterprise => {
                    return (
                        <EnterpriseTile enterprise={enterprise} key={enterprise.id} onClick={() => onSelectedEnterprise(enterprise.id)}/>
                    )
                })
            }
        </div>
    )
}

export default ListEnterprises

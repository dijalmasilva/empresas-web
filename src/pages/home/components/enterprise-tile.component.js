import './styles/enterprise-tile.style.scss';
import React from 'react';

function EnterpriseTile({enterprise, ...props}) {

    const apiUrl = process.env.REACT_APP_API_SERVER;

    return (
        <div className='enterprise-tile' {...props}>
            <div className='picture'>
                <img src={`${apiUrl}${enterprise.photo}`} alt={`logo-${enterprise.enterprise_name}`}/>
            </div>
            <div className="description">
                <h3>{enterprise.enterprise_name}</h3>
                <h5>{enterprise.enterprise_type.enterprise_type_name}</h5>
                <h6>{`${enterprise.country} - ${enterprise.city}`}</h6>
            </div>
        </div>

    )
}

export default EnterpriseTile

import './home.style.scss';

import React from 'react';
import Header from '../../components/header/header.component';
import LoadingComponent from '../../components/loading/loading.component';
import {enterpriseService} from '../../services/enterprise/enterprise.service';
import {withAuthentication} from '../../hoc/authentication.hoc';
import ListEnterprises from './components/list-enterprises.component';
import Axios from 'axios';
import {SimpleStore} from '../../utils/simple-store.util';

function HomePage(props) {

    const [enterprises, setEnterprises] = React.useState(null);
    const [textSearch, setTextSearch] = React.useState(SimpleStore.getTextSearch());
    const [flagEnterpriseType, setFlagEnterpriseType] = React.useState(SimpleStore.getFilterSearch());
    const [types, setTypes] = React.useState({});
    const [loading, setLoading] = React.useState(false);

    //carrega as tags na primeira execucao do component
    React.useEffect(() => {
        const source = Axios.CancelToken.source();
        enterpriseService.getEnterprises(source.token).then(res => {
            const allEnterprises = res.data.enterprises;
            const tags = {};
            allEnterprises.forEach(e => {
                tags[e.enterprise_type.id] = e.enterprise_type;
            });
            setTypes(tags);
        }).catch(err => {
            console.error(err);
        });

        return () => source.cancel();
    }, []);

    // carrega as empresas sempre que houver alteracao no texto de busca ou na flag do tipo de atuacao da empresa
    React.useEffect(() => {
        if (textSearch || flagEnterpriseType) {
            setLoading(true);
            const source = Axios.CancelToken.source();
            enterpriseService.getEnterprisesByFilter({
                name: textSearch,
                enterprise_types: flagEnterpriseType ? flagEnterpriseType.id : null,
            }, source.token).then(res => {
                const enterprisesResult = res.data.enterprises;
                setEnterprises(enterprisesResult);
                setLoading(false);
            }).catch(err => {
                console.error(err);
                setLoading(false);
            });
            SimpleStore.saveStateSearch(textSearch, flagEnterpriseType);
            return () => source.cancel();
        }
    }, [textSearch, flagEnterpriseType]);

    //poderia tambem estar enviando o objeto, mas nao optei para que fosse possivel consumir o outro endpoint.
    const seeEnterpriseDetail = id => {
        props.history.push(`/enterprise/${id}`)
    };

    const onSelectFilter = type => {
        if (type.id === (flagEnterpriseType ? flagEnterpriseType.id : null)) {
            setFlagEnterpriseType(null)
        } else {
            setFlagEnterpriseType(type);
        }
    };

    const builderFlagsByEnterprises = () => {
        const buttons = [];
        const keysTypes = Object.keys(types);
        keysTypes.forEach(key => {
            console.log('key: ', key);
            console.log('flagEnterprise: ', (flagEnterpriseType ? flagEnterpriseType.id : null));
            buttons.push(
                <button key={key} onClick={() => onSelectFilter(types[key])}
                        className={`flag-enterprise-type ${parseInt(key) === (flagEnterpriseType ? flagEnterpriseType.id : null) ? 'selected' : ''}`}>
                    {types[key].enterprise_type_name}
                </button>
            );
        });

        return buttons;
    };

    return (
        <div className="home-page">
            <Header onSearch={setTextSearch} initialValueSearch={textSearch} searchIsHidden={!textSearch}/>
            <div className="content">
                {
                    (loading || enterprises === null) &&
                    <div className="enterprises-init">
                        {
                            !loading &&
                            <h3>Clique na busca para iniciar.</h3>
                        }
                        <LoadingComponent visible={loading}/>
                    </div>
                }
                {
                    !loading && enterprises && enterprises.length === 0 &&
                    <div className="enterprises-no-result">
                        <h3>Nenhuma empresa foi encontrada para a busca realizada.</h3>
                    </div>
                }
                {
                    !loading && enterprises && enterprises.length > 0 &&
                    <div>
                        <div className="filters">
                            <h6>Filtrar pelo tipo de atuação: </h6>
                            {
                                builderFlagsByEnterprises()
                            }
                        </div>
                        <ListEnterprises dataSource={enterprises} onSelectedEnterprise={seeEnterpriseDetail}/>
                    </div>
                }
            </div>
        </div>
    );
}

export default withAuthentication(HomePage)

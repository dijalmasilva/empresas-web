import './header.style.scss';

import React from 'react';
import InputCustom from '../input-custom/input-custom.component';
import {withRouter} from 'react-router-dom';

function Header({onSearch, isDetailEnterprise = false, nameEnterprise, initialValueSearch, searchIsHidden = true, ...props}) {

    const [searchHidden, setSearchHidden] = React.useState(searchIsHidden);

    const onChangedText = (value) => {
        onSearch(value);
    };

    const backPage = () => {
        props.history.goBack();
    };

    return (
        <div className="header">
            {
                !isDetailEnterprise && searchHidden &&
                <div className="no-search">
                    <div className="logo-nav">
                        <img src='/images/logo-nav/logo-nav.png' alt='logo-nav'/>
                    </div>
                    <div className="icon-search">
                        <img src='/images/ic-search-copy/ic-search-copy@3x.png' alt='icon-search'
                             onClick={() => setSearchHidden(false)}/>
                    </div>
                </div>
            }
            {
                !isDetailEnterprise && !searchHidden &&
                <div className="searching">
                    <InputCustom type="text" initialValue={initialValueSearch} autoFocus onChanged={onChangedText} placeholder='Pesquisar'
                                 validator={() => null}
                                 className="input-search"
                                 prefix={
                                     <img src='/images/ic-search-copy/ic-search-copy.png' alt="icon-search" width={20}/>
                                 }
                                 suffix={
                                     <img className="bt-close" src='/images/ic-close/ic-close.png' alt="icon-close"
                                          width={14} onClick={() => setSearchHidden(true)}/>
                                 }/>
                </div>
            }
            {
                isDetailEnterprise &&
                <div className="detail-enterprise">
                    <img src='/images/left-arrow/left-arrow.png' alt='back-icon' className="back-icon"
                         onClick={backPage}/>
                    <h1>{nameEnterprise}</h1>
                </div>
            }
        </div>
    )
}

export default withRouter(Header);

import './loading.style.scss';
import React from 'react';

/**
 * @return {null}
 */
function LoadingComponent({visible = false, backdrop = false, size = '80px'}) {

    if (visible) {
        return (
            <div className={`loading ${backdrop ? 'backdrop' : ''}`}>
                <img src="/images/loading/rolling.svg" width={backdrop ? '150px' : size} alt="loading"/>
            </div>
        );
    }

    return null;
}

export default LoadingComponent

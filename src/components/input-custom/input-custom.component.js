import './input-custom.style.scss';
import React from 'react';

function InputCustom({suffix, type, prefix, onChanged, validator, initialValue = '', placeholder, className, autoFocus = false}) {

    const [error, setError] = React.useState(null);

    const handleOnChanged = e => {
        setError(validator(e.target.value));
        onChanged(e.target.value);
    };

    return (
        <div className={`div-input-custom ${className ? className : ''}`}>
            <div className={`input-custom ${error ? 'error' : ''}`}>
                {
                    prefix &&
                    <div className="prefix">
                        {prefix}
                    </div>
                }
                <div className="input">
                    <input type={type} defaultValue={initialValue} onChange={handleOnChanged}
                           placeholder={placeholder} autoFocus={autoFocus}/>
                </div>
                {
                    suffix &&
                    <div className="suffix">
                        {suffix}
                    </div>
                }
            </div>
            {
                error && <h6 className="input-error">{error}</h6>
            }
        </div>
    );

}

export default InputCustom

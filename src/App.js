import './App.scss';

import React from 'react';
import {
    Router,
    Switch,
    Route,
    Redirect
} from 'react-router-dom';
import dotenv from 'dotenv'
import LoginPage from './pages/login/login.page';
import HomePage from './pages/home/home.page';
import EnterprisePage from './pages/enterprise/enterprise.page';
import historyCustom from './history'

dotenv.config();

function App() {
    return (
        <div className="App">
            <Router history={historyCustom}>
                <Switch>
                    <Route path="/login">
                        <LoginPage/>
                    </Route>
                    <Route path="/home">
                        <HomePage/>
                    </Route>
                    <Route path="/enterprise/:id">
                        <EnterprisePage/>
                    </Route>
                    <Route path="*">
                        <Redirect to="/login"/>
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;

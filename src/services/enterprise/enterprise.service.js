import {http} from '../../utils/http.utils';
import {PATH_ENTERPRISE} from '../../utils/constants.utils';

const getEnterprise = (id, cancelToken) => {
    // return Promise.resolve({
    //     data: getEnterprisesMocked().find(e => e.id === parseInt(id)),
    // });
    return http.get(`${PATH_ENTERPRISE}/${id}`, {cancelToken: cancelToken})
};

const getEnterprises = (cancelToken) => {
    // return Promise.resolve({
    //     data: getEnterprisesMocked(),
    // });
    return http.get(PATH_ENTERPRISE, {cancelToken: cancelToken});
};

const getEnterprisesByFilter = ({enterprise_types, name}, cancelToken) => {
    // return Promise.resolve({
    //     data: getEnterprisesMocked().filter((e) => e.name.toLowerCase().includes(filter.name.toLowerCase())),
    // });

    const paramsFilter = {
        enterprise_types,
        name,
    };
    return http.get(`${PATH_ENTERPRISE}`, {params: paramsFilter, cancelToken: cancelToken});
};

export const enterpriseService = {
    getEnterprise,
    getEnterprises,
    getEnterprisesByFilter,
};

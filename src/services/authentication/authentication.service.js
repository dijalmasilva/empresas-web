import {http} from '../../utils/http.utils';
import {PATH_SIGN_IN} from '../../utils/constants.utils';

const signIn = (user, cancelToken) => {
    return http.post(PATH_SIGN_IN, user, {
        cancelToken: cancelToken,
    });
};

export const authenticationService = {
    signIn,
};
